Please find in this repository some python scripts generating KPIs to monitor software factory quality.
These scripts are invoking REST Api to retrieve data and store them in files. Then we rely on Pandas to extract valuable data.
Because REST invokation is a coslty process options are available to bypass REST call by reusing former generated file.

If you want to execute on of these scripts you can just install anaconda python and copy/paste code in it.