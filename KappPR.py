from collections import Counter
import matplotlib.pyplot as plt
from datetime import datetime
import seaborn as sns
import pandas
import requests
import json
import time
import matplotlib.patches as mpatches
# Adapt upper case variables below depending of usage
MAX_JOBS=False # Max number of considered jobs
RELOAD_FROM_JENKINS = True # Set to false if you want to rely on preloaded file
START_DATE = datetime.strptime("28/12/2021", "%d/%m/%Y") # set "startDate = 0" if you want to ignore it
END_DATE = datetime.strptime("15/01/2022", "%d/%m/%Y") # set "endDate = 0" if you want to ignore it
TEMP_STORAGE_FILE = '/Users/pierre.foyard/Downloads/jenkinsKAppPR.json'
JENKINS_URL = 'http://jenkyriba.kod.kyriba.com/job/KApp_pull_request_pipeline'


pandas.set_option('display.max_rows', None)
TITLE_SIZE = 50
ratioSuccess = 0
ratioFailure = 0
ratioUnstable = 0
rationCompilation = 0
ratioTestSetup = 0
ratioBuilDocker = 0
ratioSonar=0

# Function retrieving list of Jenkins jobs
def getJobs(url):
    response = requests.request("GET", url + '/api/json?tree=jobs[name,url,builds[number,result,duration,url,timestamp]]')    
    data = response.json()['jobs']    
    
    if MAX_JOBS > 0:        
        return data[0:MAX_JOBS]
    else:
        return data

# Retrieve Jenkins build details as sub stages
def getBuildDetails(url):
    try:
        response = requests.request("GET", url+'/wfapi/')
    except (requests.exceptions.ConnectionError, ValueError):
        return {}
    else:    
        stages = response.json()['stages']
        cleanedStages = []
        for stage in stages:
            cleanedStage = {}
            cleanedStage['name'] = stage['name']
            cleanedStage['status'] = stage['status']
            cleanedStage['durationMillis'] = stage['durationMillis']
            if 'error' in stage:
                cleanedStage['error'] = stage['error']                
            cleanedStages.append(cleanedStage)
            # We stop storing stages as soon as an error has been found
            if (cleanedStage['status']=='FAILED'):
                break
        return cleanedStages

# Test if given job contains at list one build in given date interval
def isJobInDateFrame(job, startDate, endDate):
    if (not startDate) and (not endDate):
        return True
    else:
        for build in job['builds']:
            buildDate = datetime.fromtimestamp(build['timestamp']//1000)
            startDateIsValid = (not startDate) or (buildDate >= startDate)
            endDateIsValid = (not endDate) or (buildDate <= endDate)
            if (startDateIsValid and endDateIsValid):
                return True
    return False
            
# Function retrieving list of Jenkins jobs with details on underlying stages
def getJobsWithDetails(url, startDate, endDate):
    jobs = getJobs(url)
    result = {}
    result['jobs'] = []
    if startDate:
        result['startDate'] = startDate.strftime("%d/%m/%Y")
    if endDate:
        result['endDate'] = endDate.strftime("%d/%m/%Y")

    result['url'] = url

    for job in jobs:
        if (isJobInDateFrame(job, startDate, endDate)):
            for build in job['builds']:
                urlBuild = build['url']                                    
                time.sleep(1) # Wait to avoid network issues
                build['details'] = getBuildDetails(urlBuild)        
            result['jobs'].append(job)
            
    return result  

def getStatusColor(status):    
    if status == 'FAILURE':
        return 'crimson'
    elif status == 'UNSTABLE':
        return 'sandybrown'
    elif status == 'SUCCESS':
        return 'mediumseagreen'
    else:
        return 'slategray'
    
    
def displayDuration(buildsNormalized, considerIgnored):
    stagesNormalized = pandas.json_normalize(buildsNormalized['details'].explode())
    stagesNormalized = stagesNormalized[stagesNormalized['durationMillis'].notna()]
    if not considerIgnored:
        stagesNormalized = stagesNormalized[stagesNormalized['status']!='NOT_EXECUTED']
        
    stagesNormalized['durationMillis'] = stagesNormalized['durationMillis'].apply(lambda x: round((x/1000)/60, 2))

    stagesNormalized = stagesNormalized.rename(columns={'durationMillis':'duration(mn)'})    
    #print (stagesNormalized[stagesNormalized['name'] == 'Quality Gate'].describe())
    averageDuration = stagesNormalized.groupby('name').agg({'duration(mn)': ['median','max']})
    averageDuration = averageDuration.sort_values([('duration(mn)', 'median')], ascending=False).head(20)    
    title = "Stages duration with"
    if considerIgnored:
        title = title + " every status"
    else:
        title = title + "out ignored stages"
    averageDuration.tail(20).plot.barh(title=title, stacked=True, color=['cornflowerblue', 'pink'])
    plt.show()

def default(elements, key):
    if (key in elements):
        return elements[key]
    else:
        return 0

def getStatusRatio(statusDistribution, result):
    if result in statusDistribution.index.values:
        return round(statusDistribution[result] * 100 / statusDistribution.sum(), 2)
    else:
        return 0

def getListStatusColors(statusDistribution):
    colors = []
    for elem in statusDistribution.index.values:
        colors.append(getStatusColor(elem))
    return colors

##################################################
##                  MAIN CODE                   ##
##################################################
if RELOAD_FROM_JENKINS == True:
    data = getJobsWithDetails(JENKINS_URL, START_DATE, END_DATE)
    with open(TEMP_STORAGE_FILE, 'w') as outfile:
        json.dump(data, outfile)

with open(TEMP_STORAGE_FILE) as f:
    d = json.load(f)

    
buildsNormalized = pandas.json_normalize(d['jobs'], record_path ='builds', meta =['name'])

# Display distribution of different results (FAILURE, SUCCESS...)
statusDistribution = buildsNormalized.groupby('result').size().sort_values(ascending=False)
ratioSuccess = getStatusRatio(statusDistribution, 'SUCCESS')
ratioUnstable = getStatusRatio(statusDistribution, 'UNSTABLE')
ratioFailure = getStatusRatio(statusDistribution, 'FAILURE')
pieColors = getListStatusColors(statusDistribution)
statusDistribution.plot.pie(startangle=90, 
                            shadow=True,
                            autopct='%1.2f%%',
                            title="Build status distribution:",
                            colors = pieColors)
plt.show()

# Display distribution of stages where build failed
stagesWithStatus = buildsNormalized[buildsNormalized['result']=='FAILURE']
stagesNormalized = pandas.json_normalize(stagesWithStatus['details'].explode())    
filterStatus = (stagesNormalized['status'] == 'FAILED')
stages = stagesNormalized[filterStatus].groupby('name').size().sort_values(ascending=True)    

ratioCompilation = round(ratioFailure * (default(stages,'Build functional classes') \
                                       + default(stages,'Build TS') \
                                       + default(stages,'Build TF') \
                                       ) / stages.sum(), 2)
ratioBuilDocker = round(ratioFailure*default(stages,'Create KS container')/stages.sum(), 2)
ratioSonar = round(ratioFailure*(default(stages, 'SonarQube analysis') \
                               + default(stages, 'Quality Gate')) / stages.sum(), 2)
ratioTestSetup = 0
for name, value in stages.items():
    if (name.startswith('[test]')):
        ratioTestSetup += value
ratioTestSetup = round(ratioFailure*ratioTestSetup/stages.sum(), 2)
    
stages.plot.barh(edgecolor='none', 
                 color=getStatusColor('FAILURE'), width=0.5,
                 title='Failing stages for jobs with status FAILURE')
plt.show()

# Display stages average duration ignoring skipped stages
displayDuration(buildsNormalized, False)

# Compute average number and duration of build before success
buildToSuccess = round(buildsNormalized.groupby("name").size().mean(), 2)
buildToSuccessDuration = round(buildsNormalized.groupby("name")["duration"].agg(sum).mean() / 60000, 2)

# Display KPIs:
print ('------------------------------------------')
print ("Considered starting date: ", d['startDate'])
print ("Considered end date: ", d['endDate'])
print ("Number of considered jobs: ", len(d['jobs']))
print ("Number of considered builds: ", len(buildsNormalized))
print ('------------------------------------------')
print ()
print ("Build median duration (mn):".ljust(TITLE_SIZE), \
       round(buildsNormalized['duration'].median() / 60000, 2), "mn")
print ("Successful build median duration (mn):".ljust(TITLE_SIZE), \
       round(buildsNormalized[buildsNormalized['result']=='SUCCESS']['duration'].median() / 60000, 2), "mn")
print ("Average number of build per PR:".ljust(TITLE_SIZE), \
       buildToSuccess)
print ("Average build duration per PR:".ljust(TITLE_SIZE), \
       buildToSuccessDuration, 'mn (cumulated time of every build on a PR)')
print ("Kapp PR success ratio:".ljust(TITLE_SIZE), ratioSuccess, ' %')
print ("Probability to fail at compilation:".ljust(TITLE_SIZE), \
       ratioCompilation, '% --> This statistic requires dev team investigation')
print ("Probability to fail at test setup:".ljust(TITLE_SIZE), \
       ratioTestSetup, ' % --> This statistic should be improved by New CI')
print ("Probability to fail at KS container creation:".ljust(TITLE_SIZE), \
       ratioBuilDocker, ' % --> This statistic should be improved by New CI')
print ("Probability to fail on Sonar:".ljust(TITLE_SIZE), \
       ratioSonar, ' % --> No need to improve but maybe could be executed by night')
print ("Probability to have test detecting regression:".ljust(TITLE_SIZE), \
       ratioUnstable, '% --> This statistic should be improved by QA or dev team')
print ("Probability to fail at other steps: ".ljust(TITLE_SIZE), \
       round(100-ratioSuccess-ratioCompilation-ratioTestSetup-ratioBuilDocker-ratioSonar-ratioUnstable,2), \
       '% --> This statistic can be partially improved by infrastructure reliability')